import sqlalchemy
import socket
import datetime
from sys import argv
from sqlalchemy import exc

def createTable(name):
    statement = "CREATE TABLE IF NOT EXISTS "+name+" (id serial, date VARCHAR(45) NULL,host VARCHAR(45) NULL,ip VARCHAR(45) NULL,status VARCHAR(45) NULL, description VARCHAR(350) NULL, PRIMARY KEY (id));"
    return statement

def readLines(path_to_file):
    with open(path_to_file, "r") as f:
        return f.read().splitlines()

def getHost():
    return socket.gethostname()

def getIp():
    return socket.gethostbyname(getHost())

def formList(file):
    host = getHost()
    ip = getIp()
    list = []
    for line in readLines(file):
        items = line.split(" ", maxsplit=4)
        if len(items) > 3:
            date = items[0]
            status = items[2]
            description = items[4]
            values = [date, host, ip, status, description]
            list.append(values)
    return list

def sendToDB(tableName):
    query = "INSERT INTO "+tableName+" (date, host, ip, status, description) VALUES (%s, %s, %s, %s, %s)"
    return query

def connection():
    script, first, second = argv
    table_name = 'geocitizen_logs'
    x = datetime.datetime.now()
    date = x.strftime("%Y-%m-%d")

    path_to_file = second + '/catalina.'+date+'.log'

    db_string = 'postgres+psycopg2://postgres:postgres@' + first + '/ss_demo_1'

    try:
        db = sqlalchemy.create_engine(db_string)
        db.execute(createTable(table_name))
        db.execute(sendToDB(table_name), formList(path_to_file))

        print('Logs were parsed successfully')
    except exc.SQLAlchemyError:
        print('Failed to parse logs')

if __name__ == '__main__':
    connection()